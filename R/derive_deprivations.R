water.deprivation.classify.cases <- function(type.of.source, time.to.source, vector.of.labels){
    these.are.deprived <- which(type.of.source %in% vector.of.labels | time.to.source > 30)
    ## Not deprived: source is defined and not classified as deprived and time is either NA or at most 30
    ## time can be NA if water source is located "in the dwelling"
    these.are.not.deprived <- which((is.na(type.of.source) == FALSE) &
                                    (type.of.source %in% vector.of.labels == FALSE) &
                                    (is.na(time.to.source) | time.to.source <= 30))
    water.deprived <- rep(NA, times=length(type.of.source))
    water.deprived[these.are.not.deprived] <- FALSE
    water.deprived[these.are.deprived] <- TRUE
    return(water.deprived)
}

water.deprivation.classify.cases.do.it <- function(severely.deprived.water){
    water <- get(load("water.RData"))
    severe.water.deprivation <- water.deprivation.classify.cases(
        type.of.source = water$water,
        time.to.source = water$time.to.water,
        vector.of.labels = severely.deprived.water)
    full.water <- data.frame(severe.water.deprivation,
                             type.of.source = water$water,
                             time.to.source = water$time.to.water,
                             PersonID.unique = water$PersonID.unique)
    save(full.water, file = "severe.water.deprivation.with.source.data.RData")
    save(severe.water.deprivation, file = "severe.water.deprivation.RData")
    my.log.f(paste(Sys.time(), "Successfully classified deprivation for water."))
}

my.classify.cases <- function(values.to.classify,
not.deprived, moderately.deprived, severely.deprived) {
  result <- rep(NA, times=length(values.to.classify))
  result[which(values.to.classify %in% c(not.deprived, moderately.deprived))] <-
  FALSE
  result[which(values.to.classify %in% severely.deprived)] <- TRUE
  return(result)
}

sanitation.deprivation.classify.cases.do.it.f <- function(non.deprived.sanitation, less.severely.deprived.sanitation, severely.deprived.sanitation){
toilet <- get(load("toilet.RData")) ## toilet$toilet has the data
severe.sanitation.deprivation <- my.classify.cases(toilet$toilet,
       not.deprived = non.deprived.sanitation,
       moderately.deprived = less.severely.deprived.sanitation,
       severely.deprived = severely.deprived.sanitation)
save(severe.sanitation.deprivation, file="severe.sanitation.deprivation.RData")
    my.log.f(paste(Sys.time(), "Successfully classified deprivation for sanitation."))
}

shelter.deprivation.classify.cases.do.it.f <- function(non.deprived.shelter, severely.deprived.shelter){
load("floor.RData")
severe.shelter.deprivation <- my.classify.cases(floor$floor,
       not.deprived = non.deprived.shelter,
       moderately.deprived = NULL,
       severely.deprived = severely.deprived.shelter)
save(severe.shelter.deprivation, file="severe.shelter.deprivation.RData")
    my.log.f(paste(Sys.time(), "Successfully classified deprivation for shelter."))
}
