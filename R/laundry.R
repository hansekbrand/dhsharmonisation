## Harmonize the spelling, where needed
.simpleCap.vec <- function(y){
    sapply(y, .simpleCap)
}
.simpleCap <- function(x) {
    s <- strsplit(x, " ")[[1]]
    paste(toupper(substring(s, 1, 1)), substring(s, 2),
          sep = "", collapse = " ")
}

add.missing.district.names <- function(source, RegionID, district.code){
    district.names <- identifier <- NULL
    load(file = system.file("district_names.RData", 
                            package="globallivingconditions")) ## district.names
    original.order <- data.table(identifier=paste(RegionID, district.code, sep = "."))
    foo.temp <- data.table(source, RegionID, district.code)
    setkey(foo.temp, source, RegionID, district.code)    
    foo <- unique(foo.temp)
    setkey(foo, source, RegionID, district.code)
    bar <- merge(x=foo, y=district.names, all.x=TRUE, all.y=FALSE)
    bal <- unique(bar)
    bal[, identifier := paste(RegionID, district.code, sep = ".")]
    return(as.character(bal$District[match(original.order$identifier, bal$identifier)]))
}

## wash variables with attribute value.labels,
## convert into factors, or plain numbers

## This function (numeric.from.named.atomic) numeric.from.named.atomic()
## touches only encoded values which have
## a "value.label". It will (silently) ignore purely numerical values. Another way
## of expressing the same thing special.recodes must be a character vector.

raw.from.named.atomic <- function(my.df, variable.name, s=NULL,
              special.recodes=NULL){
    return(my.df[[variable.name]]) }

numeric.from.named.atomic <- function(my.df, variable.name, s=NULL,
              special.recodes=NULL){
    ## If s is given, it should be a list with an element "standard.recodes"
    ## which must a be string. data.laundry.input works as s.

    ## if the variable is missing, return NA
    if(is.na(match(variable.name, names(my.df)))){
        return(rep(NA, times = nrow(my.df)))
    }
    ## if the variable exists but holds only missing data return NA (strip meta data)
    if(length(which(is.na(my.df[[variable.name]]))) == nrow(my.df)){
        return(rep(NA, times = nrow(my.df)))
    }

    ## apply the metadata specifications, if any
    these.are.special <- which(my.df[[variable.name]] %in%
                               attr(my.df[[variable.name]], "value.labels"))
    if(length(these.are.special) > 0){
        ## classify according to the standard recodes, and
        ## the special recodes, if any.
        if(is.null(s) == FALSE){
            if(is.null(special.recodes) == FALSE && special.recodes != ""){
                ## both standard and special recodes
                recodes <- paste(s$standard.recodes, special.recodes, sep = ";")
            } else {
                ## only standard recodes
                recodes <- s$standard.recodes
            }
        } else {
            if(is.null(special.recodes) || special.recodes == ""){
                ## Assume all numeric values without labels are good and that
                ## all labels only specify different causes of missing data, 
                ## e.g. line number of mother HC60.
                ## my.log.f(paste0("Assuming value lable presence indicates NA",
                ##      " for variable ", variable.name, " with value label: ", 
                ##      names(attr(my.df[[variable.name]], "value.labels")),
                ##      ", code: ", attr(my.df[[variable.name]], "value.labels")),
                ##      absolute.log.filename)
                my.df[[variable.name]][these.are.special] <- NA
                return(as.numeric(my.df[[variable.name]]))
            } else {
                ## only special recodes
                recodes <- special.recodes
            }
        }
        numeric <- attr(my.df[[variable.name]], "value.labels")
        strings <- names(numeric)[match(
            my.df[[variable.name]][these.are.special], numeric)]
        numeric.again <- car::recode(strings, recodes)
        my.df[[variable.name]][these.are.special] <- numeric.again
    }
    return(as.numeric(my.df[[variable.name]]))
}

factor.from.named.atomic <- function(my.df, variable.name, s=NULL,
          special.recodes=NULL){
    ## If s is given, it should be a list with an element "standard.recodes"
    ## which must a be string.

    ## if the variable is missing, return NA
    if(is.na(match(variable.name, names(my.df)))){
        return(rep(NA, times = nrow(my.df)))
    }
    ## If the variable is present, but only consist of missing data,
    ## then return NA
    if(length(which(is.na(my.df[[variable.name]]) == FALSE)) == 0){
        return(rep(NA, times = nrow(my.df)))
    }
    if(is.null(s) == FALSE){
        if(is.null(special.recodes) == FALSE && special.recodes != ""){
            ## both standard and special recodes
            recodes <- paste(s$standard.recodes, special.recodes, sep = ";")
        } else {
            ## only standard recodes
            recodes <- s$standard.recodes
        }
    } else {
        if(is.null(special.recodes) || special.recodes == ""){
            ## no recodes at all
            recodes <- NULL
        } else {
            ## only special recodes
            recodes <- special.recodes
        }
    }
    interpreted.values <- attr(my.df[[variable.name]], "value.labels")
    if(length(interpreted.values) == 0){
        ## eg, HV208 in Egypt DHS-III
        return(ifelse(my.df[[variable.name]] == 1, TRUE, FALSE))
    } else {
        if(is.null(recodes) == FALSE){
            return(car::recode(
var=names(interpreted.values)[match(my.df[[variable.name]], interpreted.values)],
recodes=recodes, as.factor=TRUE, interval="inactivate"))
        } else {
            ## No recodes, but still interpret data using the value.lables
            ## Useful when 'other' should not be set to NA (e.g. religion)
            return(factor(names(interpreted.values)[match(my.df[[variable.name]],
                                                          interpreted.values)]))
        }
    }
}

logical.from.named.atomic <- function(...){
    ifelse(factor.from.named.atomic(...)=="TRUE", TRUE, FALSE)
}

logical.compound.from.named.atomic <- function(list.of.variable.names, my.df, ...) {
    ## If there were no matches, list.of.variable.names will be of length 0
    if(length(list.of.variable.names) == 0){
        return(rep(NA, nrow(my.df)))
    }
    ## Disregard variables which have only NA:s
    only.NA.here <- which(sapply(list.of.variable.names, function(x) {
        length(which(is.na(my.df[[x]]) == FALSE)) }) == 0)
    if(length(only.NA.here) > 0){
        some.actual.measurements <- list.of.variable.names[-only.NA.here]
    } else {
        some.actual.measurements <- list.of.variable.names
    }
    if(length(some.actual.measurements) > 0){
        compound <- sapply(some.actual.measurements, function(x) {
                           logical.from.named.atomic(my.df, x, ...) } )
        if(length(some.actual.measurements) == 1){
            return(as.vector(compound))
        } else {
            return(apply(compound, 1, any, na.rm=FALSE))
        }
    } else {
        ## All variables had ony NA:s
        return(rep(NA, nrow(my.df)))
    }
}

variable.packages.f <- function(variable.packages, file.type, data.set, s){
    
    list.of.data <- sapply(variable.packages, function(package){
        if(file.exists(system.file(paste(package, file.type, "csv", sep = "."), 
                                   package="globallivingconditions"))){
            ## There is a csv-file for this package and file-type
            meta.code <- read.csv(system.file(paste(package, file.type, "csv", 
                                sep = "."), package="globallivingconditions"))
            ## my.log.f(paste(Sys.time(), "found meta.code for", package, 
            ##          "for file type", file.type), absolute.log.filename)
## collect data for each row in the meta.code
## apply() forces the data to matrix which loses type information, 
## which is why we instead use sapply() with a counter here.
            data.as.list <- sapply(1:nrow(meta.code), function(i) {
                string.to.eval <- paste0(meta.code$type[i],
 ".from.named.atomic(my.df=data.set, '", meta.code$dhs.variable.name[i],
 "', s=s, special.recodes=", '"', meta.code$special.recodes.car[i], '"', ")")
 ## my.log.f(paste(Sys.time(), "evaluating this:", string.to.eval),
 ##          absolute.log.filename)
                ## my.result <- eval(parse(text=string.to.eval))
                ## use try()
    if(length(attr(try(my.result <- eval(parse(text=string.to.eval)), silent = TRUE),
                   "condition")) > 0){
        my.log.f(paste(Sys.time(), "Failed at trying to recode stuff in variable.packages.f() i=",
                       i, "variable name = ", meta.code$dhs.variable.name[i],
                       "recodes = ", meta.code$special.recodes.car[i]))
    }

                if(length(which(is.na(my.result) == FALSE)) > 0){
    ##                 my.log.f(paste(Sys.time(), "found", 
    ## length(which(is.na(my.result) == FALSE)), "elements of non-missing data for",
    ## package, "for file type", file.type, "row number", i, "dhs variable name:", 
    ## meta.code$dhs.variable.name[i], "target variable name:", 
    ## meta.code$New.variable.name[i]), absolute.log.filename)
                    return(my.result)
                } else {
    ##                 my.log.f(paste(Sys.time(), "found only missing data for",
    ## package, "for file type", file.type, "row number", i, "dhs variable name:", 
    ## meta.code$dhs.variable.name[i], "target variable name:", 
    ## meta.code$New.variable.name[i]), absolute.log.filename)
                    ## return it anyway
                    return(my.result)
                }
              }, simplify=FALSE)
            data <- do.call(cbind, sapply(data.as.list, data.table, simplify=FALSE))
            colnames(data) <- paste(package, 
                             as.character(meta.code$New.variable.name), sep = ".")
            return(data)
        } else {
            return(NULL)
        }
    }, simplify=FALSE)
    ## as.data.frame(list.of.data[which(length(list.of.data) > 0)])
    return(Reduce(f=cbind, x=list.of.data))
}

wash.year.HV007 <- function(year, month, source, country, version){
    ## year and month are vectors, the other arguments are scalars.
    
    ## Deal with a vector of missing values.
    if(length(which(is.na(year))) == length(year)){
        ## All values are missing, use the first four characters after the space in the name of the folder
        year <- rep(as.numeric(substring(sapply(strsplit(sapply(strsplit(
            as.character(source), "/"), function(x) { x[length(x)-2] }), " "), 
            function(x) { x[2] }), 1, 4)), length(year))
    }
    
    ## 52 and 53 in Nepal (NPKR31FL.SAV) should be 96. They are 56 years ahead 
    ## of us 2052-56 = 1996
    ## 2062 and 2062 in Nepal (NP5) should be 2006 and 2007
    ## 2067 and 2068 in Nepal (NP6) should be 2011 and 2012

    ## Add 2000 to Nepal DHS III
    if(country == 524 & version == "31"){
        year <- year + 2000
    }

    ## Fix single and double digits
    ## India 1998-99 has 98, 99, and 0: => 1998, 1999, 2000; 
    ## Bangladesh 1999-00 has 99 and 0 => 1999, 2000; 
    ## Gabon 2000 has 0 and 1 => 2000, 2001
    ## 2 in Vietnam (vnkr41fl.sav)
        
    ## a single digit X means 200X
    ## these.have.one.digit <- which(nchar(year) == 1)
    these.have.one.digit <- which(nchar(as.character(year)) == 1)    
    if(length(these.have.one.digit) > 0){
        year[these.have.one.digit] <- year[these.have.one.digit] + 2000
    }

    ## two digits XY means 19XY
    ## these.have.two.digits <- which(nchar(year) == 2)
    these.have.two.digits <- which(nchar(as.character(year)) == 2)    
    if(length(these.have.two.digits) > 0){
        year[these.have.two.digits] <- year[these.have.two.digits] + 1900
    }

    ## Second, fix the calendar
    ## Nepal data sets, New years eve on 13-15 april, Month 1 starts in the middle
    ## of april. Subtract 56 or 57 years, depending on if month < 9
    if(country == 524){
        these <- which(month < 9)
        if(length(these) > 0){
            year[these] <- year[these] - 57
            year[-these] <- year[-these] - 56
        } else {
            year <- year - 56
        }
    }
    ## Add 12, or 13 to all Ethiopian datasets. New years eve is on 10th of September.
    ## Months start in september, for months 1:4, add 13, for higher months, add 12
    if(country == 231){
        these <- which(month < 5)
        if(length(these) > 0){
            year[these] <- year[these] + 9
            year[-these] <- year[-these] + 8
        } else {
            year <- year + 8
        }
    }
    
    ## Islamic calendar in Afghanistan
    ## Islamic year 1 is year 622 CE, the difference is 622-1 = 621
    if(country == 4){
        year <- year + 621
    }
    return(as.numeric(year))
}

wash.month.HV006 <- function(month, source, country, version){
    if(country %in% c(524, 231)){
        if(country == 524){
            these <- which(month < 9) ## The gregorian calender resets in month 8
            if(length(these) > 0){
                month[these] <- month[these] + 4
                month[-these] <- month[-these] - 8
            } else {
                month <- month - 8
            }
        }
        if(country == 231){
            these <- which(month < 5) ## The gregorian calender resets in month 5
            if(length(these) > 0){
                month[these] <- month[these] + 8
                month[-these] <- month[-these] - 6
            } else {
                month <- month - 6
            }
        }
    }
    return(month)
}

## any() returns FALSE on NA, if na.rm=TRUE
## and returns NA on c(NA, FALSE) if na.rm=FALSE
my.any <- function(log.vec, ...){
    if(length(which(is.na(log.vec) == FALSE)) == 0){
        return(NA)
    } else {
        return(any(log.vec, ...))
    }
}

remove.bad.chars <- function(vector.of.chars){
    ## ff is not a valid byte in UTF8, but it appears in at least one label
    ## <ff>Piped<20>into residence in CIPR3AFL (Ivory Coast, DHS-III, 1998-1999)
    sapply(vector.of.chars, function(x) {
        bar <- charToRaw(x)
        if(any(which(bar == "ff"))){
            return(rawToChar(bar[-which(bar == 'ff')]))
        } else {
            return(x)
        }
    })
}
