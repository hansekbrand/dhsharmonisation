\docType{data}                                                                                                    
\name{iwi.to.dhs}                                                                                           
\alias{iwi.to.dhs}                                                                                          
\title{A mapping between filenames}                                                           
\format{A data frame with two variables and 35 observations}                                                      
\source{                                                                                                          
  Own comparisons.
}                                                                                                                 
\description{
  The mapping is useful to map dataset from DHS to IWI, the
  International Wealth Index.
}                                                                                                                 
\author{                                                                                                          
  Hans Ekbrand
}                                                                                                                 
