\docType{data}                                                                                                    
\name{country.codes.map}                                                                                           
\alias{country.codes.map}                                                                                          
\title{A mapping between country codes}                                                           
\format{A data frame with two variables and 77 observations}                                                      
\source{                                                                                                          
  Own comparisons.
}                                                                                                                 
\description{
  The mapping is useful to map dataset from DHS to other data sources.
  DHS uses a non-standard two letter country code. This data frame
  provides a map between these codes and the numeric
  ISO-3166-alpha-3 standard for country codes
}                                                                                                                 
\author{                                                                                                          
  Hans Ekbrand
}                                                                                                                 
\usage{country.codes.map}                                                                                          
