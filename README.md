[TOC]
# README #

## What is this repository for? ##

With this R package, I want to help researchers by providing harmonised surveydata useful for analysis of poverty, development, and 
more generally, global living conditions.

1.0 Release date 2019-02-22

## Introduction

I'm in a hurry, what are the instructions for a minimal data set?

    install.packages("devtools")
    library(devtools)
	install_bitbucket(repo = "hansekbrand/iwi")
    install_bitbucket(repo = "hansekbrand/DHSharmonisation")
    library(globallivingconditions)
    dt <- download.and.harmonise(dhs.user="", <-- your username at DHS goes here
                           dhs.password="", <-- your password at DHS goes here
                           log.filename="living-conditions.log")

`dt` will now have a minimal data set without external sources. Note that if you have access to many countries `download.and.harmonise()` will take many hours (Even a single country can take an hour).

Read on if you need more than that.

## How to install the package "globallivingconditions" ##

Until this package is available on CRAN, I recommend install the package with `devtools`:

  A.    Install `devtools` if you do not already have it installed.

Note that if your OS *compiles* R packages (rather than copying ready-made binaries from the internet), i.e. if you are on OS X or Linux, then to build `devtools`, development files for `curl` are necessary, and supplied in Debian GNU/Linux by the package `libcurl4-gnutls-dev`. `devtools` also depends on `openssl` which requires development files included in `libssl-dev`. `globallivingconditions` depends on `XML` which has `libxml2-dev` as a build dependency, and `rgdal` which in turn depends on `libgdal-dev` and `libproj-dev`. Some package depends on `lme4` which in turn depends on `nloptr` which has build dependency `cmake`. The `keyring` package build depends on `libsecret-1-dev`. For Debian GNU/Linux the following command will make sure that the dependencies are satisfied:

    apt-get install libcurl4-gnutls-dev libssl-dev libxml2-dev libgdal-dev libproj-dev cmake libsecret-1-dev

Issue this within R

    install.packages("devtools")
    library(devtools)

  B.    Use `devtools` to install "globallivingconditions"

Issue this within R

    install_bitbucket(repo = "hansekbrand/iwi")
	install_bitbucket(repo = "hansekbrand/DHSharmonisation")

This will automatically install all dependencies, recursively. Note that if you have not installed `Rtools`, `devtools` will issue a harmless warning, which you safely can ignore. On Microsoft Windows that warning will read like this:

	WARNING: Rtools is required to build R packages, but is not currently installed.

	Please download and install Rtools 4.0 from https://cran.r-project.org/bin/windows/Rtools/.

## Deployment instructions ##

I have found it convenient to have my credentials stored in a file which can be loaded by many different scripts. It also makes it possible to share scripts with others without sharing your secret credential.

    credentials <- list(dhs.user = "foo@bar.com", dhs.password = "r3al.password")
    save(credentials, file = paste(normalizePath("~"), "user_credentials_dhs.RData", sep = "/"))
    
This way, the credentials can be loaded regardless of the current directory

    library(globallivingconditions)
    load(paste(normalizePath("~"), "user_credentials_dhs.RData", sep = "/"))
    	  # this must be a list with two named elements:
          # "dhs.user" and "dhs.password" 
    my.dt <- download.and.harmonise(
        dhs.user=credentials$dhs.user,
        dhs.password=credentials$dhs.password,
        countries = c("Nepal"),
        log.filename = "living-conditions.log"
    )

## FAQ ##

### How do I get IWI-scores? ###
IWI-scores are calculated if the variable package "wealth" is requested, e.g.

    variable.packages = c("wealth")

### How do I add custom variables from the DHS? ###
The package supports adding custom variables from the DHS via a mechanism called "variable packages". A variable package requires a set of input variables, and a set of output variables (one output variable for each input variable) and can, optionally, recode the input data using recode rules in the format recognized by the `recode` function in the package `car`. A variable package reads its input from a CSV-file in the directory where `globallivingconditions` is installed. To find out where this directory resides in your installation, try:

    system.file("wealth.PR.csv", package="globallivingconditions")
	
A CSV-file for a variable package governs a specific file type in the DHS, i.e. one of "PR", "IR", "MR", "KR". The filename determines which file type the CSV-file will be applied on, and the above file "wealth.PR.csv" will be applied on "PR"-files.

Currently the package comes with variable packages for:

  - wealth: goods in the household (include this to get IWI-scores)
  - lockdown: information about access to soap, if the toilet is shared, etc.
  - weight: body weight and length
  - india: the specific weights for states in India (include this to get harmonized district names for India)
  - malaria: detailed information about the use of bednets in the household
  
To see exactly which variables are included, look into the CSV-files here https://bitbucket.org/hansekbrand/dhsharmonisation/src/master/inst/

To add custom variables from the DHS you need to create a CSV-file with the following three columns: New.variable.name, type, dhs.variable.name, special.recodes.car

This is how the lockdown variable package is defined (the first three variables of it).

    New.variable.name,type,dhs.variable.name,special.recodes.car
    "lack.soap","logical","HV232"
    "source.of.water","factor","HV201"
	"fetch.water","numeric","HV204","c('No sabe', '995', '998') = NA; c('On premises', 'on premises', 'Delivered water', 'On the spot', 'Less than  minute', 'Delivered to dwelling') = 0; c('More than 12 hours', 'One day or longer', '90 Minutes or more', '300+', '995+', '500 +') = 100"

The values in the "New.variable.name"-column are arbitrary. The "type" column specifies the target type, use "raw" if you do not want the data changed. The "special.recodes.car"-column is optional and takes a string that works as the "recodes" argument for function `recode` in package `car`, see the manual of that function for how it works. NB, use single quotation marks to delimit strings within that string. In the example above, the recode rule is used to harmonize a mix of strings values and numeric values in the input a numeric value in the output, and set some values to NA ('No sabe', '995' and '998').


### How do I avoid re-downloading files? ###
Either use an absolute path as `directory` argument to
`download.and.harmonise()`, or use a relative path which starts with
the parent directory symbol `..`.

Consider the following directory structure:

    dhs_projects
	- project1
    - project2
	dhs_static_files

Use `project1` as working directory for project 1 and use
`../../dhs_static_files` as `directory` argument to
`download.and.harmonise()`. When you start a new project, use
`project2` as working directory and, again, use `../../dhs_static_files` as
`directory`.

### Caveats when repeatedly running `download.and.harmonise()` in the same directory ###
Since some countries have very large SPSS-files, the package makes a shortcut to improve the execution speed: If a file has already been imported once from SPSS-format, it is not imported again, instead the saved file in R-format is opened. When the set of variables needed for the harmonisation changes between invocations, e.g. because the second invocation includes another set of `variable packages`, those variables will not be available since they are not in the saved file in R-format. To work around this, one can delete the directory `globallivingconditions_cached_R_files` before the second invocation.

### How do I fully automate running of the package? ###
  1. Write a batchfile in a format supported by your operating system, 
  2. Run the batchfile.
  
The most tricky part is probably to get the paths correct, which involves quite a few backslash characters. Here is a batchfile I use on Microsoft Windows, to run a knitr-file identified via an absolute path in a working dir also specified via an absolute path. By using absolute paths, the batchfile itself is not tied to a certain location on the filesystem. `chcp 65001` is needed to activate support for UTF-8 characters in the batch file.

    chcp 65001
    D:
    cd D:\\Hans\projects\defaults-clean
    "C:\Program Files\R\R-4.2.1\bin\x64\Rscript.exe" -e "library('knitr'); knit('D:\\\\OneDrive Hans\\OneDrive - Göteborgs Universitet\\Forskning\\global-living-conditions\\testing-master-defaults - clean.Rnw')"
	
By default, knitr-files use the workingdir that they are located in. To de-couple the file system location of the knitr-file `testing-master-defaults - clean.Rnw` from the working dir you intend to run it in, remember to set the `root.dir` option, e.g. by including a chunk like this in the beginning of the knitr-file.

    <<setup>>=
	knitr::opts_knit$set(root.dir = ".")
	@

To actually run the program, here is a typical chunk, suitable to include after the `<<setup>>` chunk above.

    <<load.data, echo = TRUE>>=
    library(devtools)
    install_bitbucket(repo = "hansekbrand/iwi")
    install_bitbucket(repo = "hansekbrand/DHSharmonisation")
    library(globallivingconditions)
    load(paste(normalizePath("~"), "user_credentials_dhs.RData", sep = "/"))
    my.dt <- download.and.harmonise(
      dhs.user=credentials$dhs.user,
      dhs.password=credentials$dhs.password,
      superclusters=FALSE,
      variable.packages=c("wealth", "survey_info"),
      vars.to.keep = NULL,
      directory = "../../dhs_static_files")
    save(my.dt, file = "living-conditions.RData")
    @ 
	
Note that if you use `CACHE=TRUE` as chunk-options, then make sure that the objects created in a chunk is not too large, or the caching mechanism of knitr will exit with an error.

To use particular branch of the package use the `ref` option of `install_bitbucket`, e.g. to use the branch `only_living_children`.

    install_bitbucket(repo = "hansekbrand/DHSharmonisation", ref = "only_living_children")
	
### Why is this package written in form of a knitr-document? ###

The end user will not notice that knitr is involved under the hood,
the end user will only see a function that given username and password
to the DHS-server, will return the dataset.

Perhaps we won't need knitr when we are done, but
during development, I think it is a good choice, for three reasons:

#### 1. The ability to cache work done during development. ####

We are dealing with 23 GB of data which, in many steps is refined into
a dataset of about 11.5 million rows and 130 MB of data. For clarity
this process is divided into blocks that is small enough to
comprehend. The most computationally efficient way would be to only
load one piece of data once, and do all manipulations on it right
away. However, for a human to grasp all the manipulations,
partitioning the task into small bits is much easier to understand,
modify, and debug.

knitr automates parts of the caching needed for that workflow. The low
RAM requirement (which came later in the process, long after I had
settled on knitr), reduced the strength of the caching part somewhat,
since I use external files for more and more of the caching.


#### 2. To minimise the work required to keep documentation and code in sync ####

To minimise the work required to keep documentation and code in sync,
i.e. for the same reason that knitr, or, more generally, literate
programming is a good choice for replicable research.


#### 3. The output of that document is useful for validation ####

Also, the harmonisation program is rather big, about 2200 lines of
code. If instead of knitr-document I would have made one master
function that controlled the complete process, then I would have to
explain what it did, and the kind of objects it deals with and
creates, by means of comments. With knitr, I can easily show the data
by creating a table or a graph. All tables and graphs in the PDF are
strictly speaking not necessary to achieve the harmonisation, but they
do a good job explaining what is happening, and they are essential to
validation - we can immediatly tell that this categorisation of a type
of floor is correctly or incorrectly classified.

## Who do I talk to? ##

> Hans Ekbrand <hans.ekbrand@gu.se>
> University of Gothenburg, department of sociology and work science
> Sweden
