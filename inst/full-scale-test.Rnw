\documentclass[10pt]{article}
\usepackage[paperwidth=164mm, paperheight=280mm, top=19mm, bottom=19mm, left=1cm, right=1cm]{geometry} % 16:9 three pages in a row
% \usepackage[english]{babel}
\usepackage{longtable}
\usepackage{fontspec}
\usepackage{booktabs}
\usepackage{hyperref}
\usepackage{varioref} %% vref
%% \usepackage{paralist} %% compactenum, in texlive-latex-extra
%% \usepackage{siunitx} %% \num, in texlive-science
%% \usepackage{IEEEtrantools} % proper indentation for multi-line equations, in texlive-publishers
\usepackage{amsmath} %% \pmatrix
%% \usepackage{subfig} %% in texlive-latex-recommended

%% make a single « start and end a chunk of \texttt{}, and two «« to represent the literal «.
\catcode`«=\active
\def«#1«{\texttt{#1}}

\begin{document}

The purpose for this file is to test new versions at full scale. 

This chunk is needed to make sure evaluation is taking place in the current directory.
<<setup.outer, echo=FALSE, tidy=TRUE, message=FALSE, echo = TRUE>>=
opts_chunk$set(results='hide', cache=TRUE, echo=TRUE, warning=FALSE, fig.width=4, fig.height=4, fig.pos = 'htb', tidy.opts=list(blank=FALSE, width.cutoff=50), background='white', tidy=TRUE, error=TRUE, message=FALSE, dev="png", dpi = 200, autodep = TRUE)
opts_knit$set(root.dir = ".")
options(scipen=999) ## Avoid scientific formating of large numbers.
@

To minimize the downloads, a directory for a local cache is used. This chunk defines the path to that directory. When revised versions of already downloaded survey data is downloaded, you need to manually delete the obsolete files. %% (This should really be automated by keeping a local database of which link is associated to which SPSS-files and remove files that no longer are reachable by the currently available links on the DHS website. The first two steps is to save the resulting filename from each link and to, for every maximal (ie, getting all countries/waves) session save all the available links.

<<configure.dir.show, eval=FALSE>>=
## To test the downloadning mechanism too, comment out the next line
## my.dir <- "D:/Hans/DHS/global-living-conditions"
@ 

<<configure.dir, echo=FALSE, eval=FALSE>>=
my.dir <- "global-living-conditions"

## If you have a local cache of files downloaded from the DHS website
## then change my.dir to point to that folder

if(Sys.info()["sysname"] == "Windows"){
  ## figure out which computer we are in.
  if(file.exists("D:/Hans/DHS/global-living-conditions")){
      my.dir <- "D:/Hans/DHS/global-living-conditions"    
  } else {
      if(file.exists("F:/Data/DHS")){
          my.dir <- "F:/Data/DHS"
    } else {
      stop("no data directory found in two spots for Windows.")
    }
  }
} else {
    ## 245
    if(file.exists("/media/sf_Our_data/Data/DHS")){
        my.dir <- "/media/sf_Our_data/Data/DHS"
    }
    ## SNIC
    if(file.exists("/mimer/NOBACKUP/groups/globalpoverty1/hans")){
        my.dir <- "/mimer/NOBACKUP/groups/globalpoverty1/hans/global-living-conditions"            
    }
    ## nomad, only do this for a small set
    if(file.exists("~/mnt/poverty/media/sf_Our_data/Data/DHS")){
        my.dir <- "~/mnt/poverty/media/sf_Our_data/Data/DHS"
    }
}
@

The chunk below is based from the «README« in the repository for the «globallivingconditions« package.

<<load.data>>=
library(devtools)
install_bitbucket(repo = "hansekbrand/iwi")
## install_bitbucket(repo = "hansekbrand/DHSharmonisation", upgrade = "never", ref = "memisc")
## install_bitbucket(repo = "hansekbrand/DHSharmonisation", upgrade = "never", ref = "aggregate")
install_bitbucket(repo = "hansekbrand/DHSharmonisation", upgrade = "never", ref = "sf")
library(data.table); library(globallivingconditions)
load(paste0(normalizePath("~"), "/user_credentials_dhs.RData"))
my.dt <- download.and.harmonise(
    dhs.user=credentials$dhs.user,
    dhs.password=credentials$dhs.password,
    file.types.to.download = c("PR", "GE", "GC", "IR", "MR", "KR"),
    variable.packages = c("wealth"),
    make.pdf = FALSE,
    vars.to.keep = NULL,
    keep.temporary.files = TRUE
)

save(my.dt, file = "dhs.RData")
rm(my.dt)
gc(verbose=FALSE)

@

<<debug, eval=FALSE, echo=FALSE>>=
library(knitr)
opts_chunk$set(results='hide', cache=TRUE, echo=TRUE, warning=FALSE, fig.width=4, fig.height=4, fig.pos = 'htb', tidy.opts=list(blank=FALSE, width.cutoff=50), background='white', tidy=TRUE, error=TRUE, message=FALSE, dev="png", dpi = 200, autodep = TRUE)
opts_knit$set(root.dir = ".")
options(scipen=999) ## Avoid scientific formating of large numbers.
library(devtools)
install_bitbucket(repo = "hansekbrand/iwi")
install_bitbucket(repo = "hansekbrand/DHSharmonisation", upgrade = "never", ref = "sf")
library(data.table); library(globallivingconditions)
load(paste0(normalizePath("~"), "/user_credentials_dhs.RData"))

satellite.images=FALSE
temperature = FALSE
file.types.to.download = c("PR", "GE", "GC", "IR", "MR", "KR")
log.filename = "living-conditions.log"
precipitation = FALSE
malaria = FALSE ## download data using the Malaria atlas
max.file.size.for.parallelisation = NULL
qog.vars = c("wbgi_gee", "wdi_gnicapatlcur")
temperature.file.path = NULL
qog.file.path = paste0(normalizePath("~"), "/")

dhs.user=credentials$dhs.user
dhs.password=credentials$dhs.password
vars.to.keep=NULL
countries = NULL
waves = NULL
variables.to.inherit = c("severe.education.deprivation", "education",
                         "education.in.years", "Religion", "Age.at.first.cohabitation", "caste")
variable.packages = c("wealth")
check.dhs.for.more.data = FALSE
superclusters = TRUE
my.dir <- "global-living-conditions"
directory = my.dir
make.pdf = FALSE
keep.temporary.files = TRUE

my.globals <- c("dhs.user", "dhs.password", "log.filename", "vars.to.keep", "variables.to.inherit", "countries", "waves", "file.types.to.download", "max.file.size.for.parallelisation", "make.pdf", "directory", "variable.packages", "satellite.images", "precipitation", "temperature", "malaria", "superclusters", "check.dhs.for.more.data", "qog.vars", "temperature.file.path", "qog.file.path")

## Find the frame which is our "global environment"
my.env <- NA
for(i in 0:sys.nframe()){
    content <- ls(envir = sys.frame(sys.parent(i)))
    if(length(grep("my.globals", content)) > 0){
        my.env <- i
    }
}

library(knitr); library(globallivingconditions) ## todo move these to the knitr-file?
## save the current working directory as an absolute path, so we can return.
starting.dir <- getwd()

## also create a separate file tree for derived files in the current dir, if such a directory not already exists.
## since starting dir is an absolute path, r.dir is also an absolute.path
r.dir <- paste(starting.dir, "globallivingconditions_cached_R_files", sep = "/")
if(utils::file_test("-d", r.dir) == FALSE){
    dir.create(r.dir)
}

## If the directory for raw data files does not exist, then create it
if(utils::file_test("-d", directory) == FALSE){
    dir.create(directory)
}
## if `directory` is a relative path, make it into a absolute path now that we can return to
setwd(directory)
directory <- getwd()
        
## return back to starting.dir
setwd(starting.dir)

## Actually, try running in r.dir
setwd(r.dir)

## variables in this environment (log.filename, dhs.user, etc) will be available in knit() as well.
knitr::set_parent(system.file("latex-headers.Rnw", package="globallivingconditions"))
knitr::opts_knit$set(root.dir = ".")

## Set some options for knitr()
knitr::opts_chunk$set(results='hide', cache=TRUE, echo=TRUE, warning=TRUE, fig.pos = 'htb', 
                      tidy.opts=list(blank=FALSE, width.cutoff=50), background='white',
                      tidy=FALSE, error=TRUE, message=FALSE, dev="pdf", autodep = TRUE)

if(make.pdf){
    knitr::knit2pdf(system.file("harmonise-and-merge.Rnw", package="globallivingconditions"),
                    compiler="xelatex")
} else {
    knitr::knit(system.file("harmonise-and-merge.Rnw", package="globallivingconditions"))
}

@ 

\end{document}

