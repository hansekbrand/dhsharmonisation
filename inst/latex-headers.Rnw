\documentclass[10pt,english]{article}
% \documentclass{article}
% \usepackage[paperwidth=176.7258mm, paperheight=280mm, top=19mm, bottom=19mm, left=1cm, right=1cm]{geometry} % 4:3, two pages in a row
% \usepackage[paperwidth=165mm, paperheight=182mm, top=5mm, bottom=12mm, left=1.5cm, right=1.5cm]{geometry} % 16:9 two pages in a row

\usepackage[paperwidth=164mm, paperheight=280mm, top=19mm, bottom=19mm, left=1cm, right=1cm]{geometry} % 16:9 three pages in a row
%% \usepackage[paperwidth=210mm, paperheight=297mm, top=19mm, bottom=19mm, left=1cm, right=1cm]{geometry} % A4

% \usepackage[paperwidth=151mm, paperheight=280mm, top=12mm, bottom=12mm, left=0.8cm, right=0.8cm]{geometry} % 16:10 three pages in a row
%% For paper prints
%% \usepackage[a4paper, top=19mm, bottom=19mm, left=1.5cm, right=1.5cm]{geometry} % 
\usepackage[english]{babel}
\usepackage{longtable}
\usepackage{fontspec}
\usepackage{booktabs}
\usepackage{hyperref}
\usepackage{varioref} %% vref
\usepackage{paralist} %% compactenum
%% \renewenvironment{knitrout}{\small\renewcommand{\baselinestretch}{.85}}{} %% Comment this out when images are created, or they will fail. When the images are done, uncomment again.
\begin{document}
